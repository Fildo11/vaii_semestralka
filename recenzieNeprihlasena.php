<?php
include('sendRegistracia.php');
$update = false;
$id = 0;
$subjectRecenzia = '';
$messageRecenzia = '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WebStranka</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600&
    subset=latin,latin-ext">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/moj.js"></script>
    <script src="js/vlastny.js"> </script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<header>
    <div class="contact-bar">
        <div class="container">
            <ul class="menu personal">
                <?php
                if (!isLoggedIn()) {
                    echo "<li><a href=prihlasenie.php>Prihlásiť sa </a></li>";
                    echo "<li><a href=registracia.php>Vytvoriť účet</a></li>";
                }
                ?>
                <div class="content">
                    <?php if (isset($_SESSION['success'])) : ?>
                            <h3>
                                <?php
                                echo $_SESSION['success'];
                                unset($_SESSION['success']);
                                ?>
                            </h3>
                    <?php endif ?>
                    <div class="profile_info">
                        <div>
                            <?php if (isset($_SESSION['user'])) : ?>
                                <strong><?php echo $_SESSION['user']['username']; ?></strong>
                                    <i style="color: #888;">(<?php echo ucfirst($_SESSION['user']['user_type']); ?>)</i>
                                    <br>
                                    <a href="index.php?logout='1'" style="color: blueviolet;">Odhlásiť sa</a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </ul>
            <ul class="menu date">
                <body onload="mojaFunkcia()">
                <div id="datum"></div>
                <div id="den_v_tyzdni"></div>
                </body>
            </ul>
        </div>
    </div>
    <div class="nav-bar">
        <div class="container">
            <h1 class="logo">
                <a href="#"></a>
            </h1>

            <nav class="group">
                <ul class="menu navigation">
                    <li><a href="index.php"> <i class="fa fa-home fa-2x"> </i> Ubytovanie </a></li>
                    <li><a href="rezervacie.php"> <i class="fa fa-newspaper-o fa-2x"> </i> Rezervácia </a></li>
                    <li class="selected"><a href="recenzie.php"> <i class="fa fa-comment fa-2x"> </i> Recenzie </a></li>
                    <li><a href="konto.php"> <i class="fa fa-info-circle fa-2x"> </i> Moje konto </a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<main>
    <article>
        <header class="post-header">
            <div class="container">
                <h1 class="post-title">Recenzie</h1>
            </div>
        </header>
        <br>
        <h4 class="text-center">Pre pridávanie recenzii musíte byť prihlásený.</h4>
        <div class="post-content">
            <div class="container">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">Hľadaj</span>
                        <input type="text" name="search_text" id="search_text" placeholder="Hľadaj podľa nickname"
                               class="form-control"/>
                    </div>
                </div>
                <div id="result"></div>
            </div>
        </div>
    </article>
</main>
<footer class="footer">
    <div class="container">
        <ul class="menu nav-footer">
            <li><a href="index.php"> Ubytovanie </a></li>
            <li><a href="rezervacie.php"> Rezervácia </a></li>
            <li><a href="recenzie.php"> Recenzie </a></li>
            <li><a href="konto.php"> Moje konto </a></li>
        </ul>
    </div>
</footer>
</body>
</html>