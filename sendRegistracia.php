<?php
session_start();
$db = mysqli_connect('localhost', 'root', 'dtb456', 'ubytovaci_web');
$errors   = array();

/* Registrácia nových používateľov */
if (isset($_POST['register_btn'])) {
    $username = $_POST['username'];
    $email = $_POST['email'];
    $phone_number = $_POST['phone_number'];
    $date_birthday = $_POST['date_birthday'];
    $password_1 = $_POST['password_1'];
    $password_2 = $_POST['password_2'];
    if ($password_1 != $password_2) {
        array_push($errors, "Heslá sa nezhodujú");
        echo '<script> alert("Heslá sa nezhodujú. Účet nebol vytvorený!") </script>';
        echo '<script>window.location="registracia.php" </script>';
    }
    $dupl = "SELECT username FROM ucty WHERE username='$username'";
    $result = mysqli_query($db, $dupl);
    if (mysqli_num_rows($result)>0) {
        array_push($errors, "Tento nickname už existuje");
        echo '<script> alert("Tento nickname už existuje. Účet nebol vytvorený") </script>';
        echo '<script>window.location="registracia.php" </script>';
    }
    if (count($errors) == 0) {
        $password = md5($password_1);
        if (isset($_POST['user_type'])) {
            $user_type = e($_POST['user_type']);
            $query = "INSERT INTO ucty (username, email, phone_number, date_birthday, user_type, password) 
					  VALUES('$username', '$email', '$phone_number', '$date_birthday', '$user_type', '$password')";
            mysqli_query($db, $query);
            echo '<script> alert("Boli ste úspešne zaregistrovaný!") </script>';
            echo '<script>window.location="index.php" </script>';
        }else{
            $query = "INSERT INTO ucty (username, email, phone_number, date_birthday, user_type, password) 
					  VALUES('$username', '$email', '$phone_number', '$date_birthday', 'user', '$password')";
            mysqli_query($db, $query);
            $logged_in_user_id = mysqli_insert_id($db);
            $_SESSION['user'] = getUserById($logged_in_user_id);
            echo '<script> alert("Boli ste úspešne zaregistrovaný!") </script>';
            echo '<script>window.location="index.php" </script>';
        }
    }
}

/* Funkcia vracia používateľa podľa jeho id */
function getUserById($id) {
    $db = mysqli_connect('localhost', 'root', 'dtb456', 'ubytovaci_web');
    $query = "SELECT * FROM ucty WHERE id=" . $id;
    $result = mysqli_query($db, $query);
    $user = mysqli_fetch_assoc($result);
    return $user;
}

/* Prihlásenie používateľov do aplikácie, ktorí už majú vytvorení účet */
if(isset($_POST['login_btn'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    if (empty($username)) {
        array_push($errors, "Zadajte nickname");
    }
    if (empty($password)) {
        array_push($errors, "Zadajte heslo");
    }
    if (count($errors) == 0) {
        $password = md5($password);
        $query = "SELECT * FROM ucty WHERE username='$username' AND password='$password' LIMIT 1";
        $results = mysqli_query($db, $query);
        if (mysqli_num_rows($results) == 1) {
            $logged_in_user = mysqli_fetch_assoc($results);
            if ($logged_in_user['user_type'] == 'admin') {
                $_SESSION['user'] = $logged_in_user;
                header('location: admin/home.php');
            }else{
                $_SESSION['user'] = $logged_in_user;
                header('location: index.php');
            }
        }else {
            array_push($errors, "Nesprávne meno alebo heslo");
            echo '<script> alert("Nesprávne meno alebo heslo!") </script>';
            echo '<script>window.location="prihlasenie.php" </script>';
        }
    }
}

/* Funkcia ktorá vracia či je v aplikácii aktuálne prihlásený nejaký pužívateľ */
function isLoggedIn() {
    if (isset($_SESSION['user'])) {
        return true;
    }else{
        return false;
    }
}

/* Ohlásenie aktuálne prihláseného požívateľa */
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['user']);
    echo '<script> alert("Boli ste odhlásený!") </script>';
    echo '<script>window.location="index.php" </script>';
}

/* Uloženie recenzie do tabuľky recenzie v databáze */
if(isset($_POST['saveRecenzia'])) {
    $usernameRecenzia = $_SESSION['user']['username'];
    $subjectRecenzia = $_POST['subjectRecenzia'];
    $messageRecenzia = $_POST['messageRecenzia'];
    $loginId = $_SESSION['user']['id'];
    if (empty($subjectRecenzia)) {
        array_push($errors, "Predmet je povinný");
    }
    if (empty($messageRecenzia)) {
        array_push($errors, "Správa je povinná");
    }
    if (count($errors) == 0) {
        $query = "INSERT INTO recenzie (username, subject, mess, login_id) 
					  VALUES('$usernameRecenzia', '$subjectRecenzia', '$messageRecenzia','$loginId')";
        mysqli_query($db, $query);
        echo '<script> alert("Recenzia bola pridaná!") </script>';
        echo '<script>window.location="recenzie.php" </script>';
    }
}

/* Vymazanie recenzie z tabuľky recenzie z databázy */
if (isset($_GET['delete'])) {
    $id = $_GET['delete'];
    $db->query("DELETE FROM recenzie WHERE id=$id") or die($db->error());
    echo '<script> alert("Recenzia bola vymazaná!") </script>';
    echo '<script>window.location="recenzie.php" </script>';
}

/* Editovanie recenzie v tabuľke recenzie v databáze */
if(isset($_POST['updateRecenzia'])) {
    $id = $_POST['id'];
    $nicknameRecenzia = $_SESSION['user']['username'];
    $subjectRecenzia = $_POST['subjectRecenzia'];
    $messageRecenzia = $_POST['messageRecenzia'];
    $loginId = $_SESSION['user']['id'];
    mysqli_query($db, "UPDATE recenzie SET username='$nicknameRecenzia', subject='$subjectRecenzia', 
        mess='$messageRecenzia', login_id = '$loginId' WHERE id = '$id'");
    echo '<script> alert("Recenzia bola upravená!") </script>';
    echo '<script>window.location="recenzie.php" </script>';
}
