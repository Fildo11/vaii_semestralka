<?php
include('sendRegistracia.php');
if (!isLoggedIn()) {
    echo '<script> alert("Pre vstup do svojho konta musíte byť prihlásený!") </script>';
    echo '<script>window.location="prihlasenie.php" </script>';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WebStranka</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600&
    subset=latin,latin-ext">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="jquery-3.5.1.min.js"></script>
    <script src="js/vlastny.js"></script>
    <script src="js/zobrazUdaje.js"> </script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<header>
    <div class="contact-bar">
        <div class="container">
            <ul class="menu personal">
                <?php
                if (!isLoggedIn()) {
                    echo "<li><a href=prihlasenie.php>Prihlásiť sa </a></li>";
                    echo "<li><a href=registracia.php>Vytvoriť účet</a></li>";
                }
                ?>
                <div class="content">
                    <?php if (isset($_SESSION['success'])) : ?>
                        <h3>
                            <?php
                            echo $_SESSION['success'];
                            unset($_SESSION['success']);
                            ?>
                        </h3>
                    <?php endif ?>
                    <div class="profile_info">
                        <div>
                            <?php if (isset($_SESSION['user'])) : ?>
                                <strong><?php echo $_SESSION['user']['username']; ?></strong>
                                <i style="color: #888;">(<?php echo ucfirst($_SESSION['user']['user_type']); ?>)</i>
                                <br>
                                <a href="index.php?logout='1'" style="color: blueviolet;">Odhlásiť sa</a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </ul>
            <ul class="menu date">
                <body onload="mojaFunkcia()">
                <div id="datum"></div>
                <div id="den_v_tyzdni"></div>
                </body>
            </ul>
        </div>
    </div>
    <div class="nav-bar">
        <div class="container">
            <h1 class="logo">
                <a href="#"></a>
            </h1>
            <nav class="group">
                <ul class="menu navigation">
                    <li><a href="index.php"> <i class="fa fa-home fa-2x"> </i> Ubytovanie </a></li>
                    <li><a href="rezervacie.php"> <i class="fa fa-newspaper-o fa-2x"> </i> Rezervácia </a></li>
                    <li><a href="recenzie.php"> <i class="fa fa-comment fa-2x"> </i> Recenzie </a></li>
                    <li class="selected"><a href="konto.php"> <i class="fa fa-info-circle fa-2x"> </i> Moje konto </a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<main>
    <article>
        <header class="post-header">
            <div class="container">
                <h1 class="post-title">Informácie o Vás</h1>
            </div>
        </header>
        <div class="post-content">
            <div class="container">
                <button class="btn_moj" onclick="functionOsobneInfo()">Zobraz moje osobné informácie</button>
                <div id="myInfo">
                    <?php
                    $myDB = new mysqli('localhost', 'root', 'dtb456', 'ubytovaci_web') or die (mysqli_eror($myDB));
                    $pom = $_SESSION['user']['id'];
                    $result = $myDB->query("SELECT username, email, phone_number, date_birthday FROM ucty WHERE id = $pom") or die($myDB->error);
                    ?>
                    <div class="row justify-content-center">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nickname</th>
                                <th>Email</th>
                                <th>Telefónne číslo</th>
                                <th>Dátum narodenia</th>
                            </tr>
                            </thead>
                            <?php
                            while ($row = $result->fetch_assoc()): ?>
                                <tr>
                                    <td><?php echo $row['username']; ?></td>
                                    <td><?php echo $row['email']; ?></td>
                                    <td><?php echo $row['phone_number']; ?></td>
                                    <td><?php echo $row['date_birthday']; ?></td>
                                </tr>
                            <?php endwhile; ?>
                        </table>
                    </div>
                </div>
                <br><br>
                <button class="btn_moj" onclick="functionRezervacie()">Zobraz moje rezervácie</button>
                <div id="myReservation">
                    <?php
                    $myDB = new mysqli('localhost', 'root', 'dtb456', 'ubytovaci_web') or die (mysqli_eror($myDB));
                    $pom = $_SESSION['user']['id'];
                    $result = $myDB->query("SELECT title, date_arrive, num_person, num_nights, price FROM rezervacie WHERE login_id = $pom") or die($myDB->error);
                    ?>
                    <div class="row justify-content-center">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Názov ubytovania</th>
                                <th>Dátum príchodu</th>
                                <th>Počet osôb</th>
                                <th>Počet nocí</th>
                                <th>Počet cena</th>
                            </tr>
                            </thead>
                            <?php
                            while ($row = $result->fetch_assoc()): ?>
                                <tr>
                                    <td><?php echo $row['title']; ?></td>
                                    <td><?php echo $row['date_arrive']; ?></td>
                                    <td><?php echo $row['num_person']; ?></td>
                                    <td><?php echo $row['num_nights']; ?></td>
                                    <td><?php echo $row['price']; ?> €</td>
                                </tr>
                            <?php endwhile; ?>
                        </table>
                    </div>
                </div>
                <br><br>
                <button class="btn_moj" onclick="functionRecenzie()">Zobraz moje recenzie</button>
                <div id="myRecenzie">
                    <?php
                    $myDB = new mysqli('localhost', 'root', 'dtb456', 'ubytovaci_web') or die (mysqli_eror($myDB));
                    $pom = $_SESSION['user']['id'];
                    $result = $myDB->query("SELECT subject, mess FROM recenzie WHERE login_id = $pom") or die($myDB->error);
                    ?>
                    <div class="row justify-content-center">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Predmet</th>
                                <th>Správa</th>
                            </tr>
                            </thead>
                            <?php
                            while ($row = $result->fetch_assoc()): ?>
                                <tr>
                                    <td><?php echo $row['subject']; ?></td>
                                    <td><?php echo $row['mess']; ?></td>
                                </tr>
                            <?php endwhile; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </article>
</main>
<footer class="footer">
    <div class="container">
        <ul class="menu nav-footer">
            <li><a href="index.php"> Ubytovanie </a></li>
            <li><a href="rezervacie.php"> Rezervácia </a></li>
            <li><a href="recenzie.php"> Recenzie </a></li>
            <li><a href="konto.php"> Moje konto </a></li>
        </ul>
    </div>
</footer>
</body>
</html>
