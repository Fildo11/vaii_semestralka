<?php
include('sendRegistracia.php');
if (!isLoggedIn()) {
    echo '<script> alert("Pre vytvorenie rezervácie musíte byť prihlásený!") </script>';
    echo '<script>window.location="prihlasenie.php" </script>';
}
$connect = mysqli_connect("localhost", "root", "dtb456", "ubytovaci_web");
$contain = 0;

/* Pridávanie apartmánov do nákupného košíka */
if (isset($_POST["add_to_cart"])) {
    if ($contain == 0) {
        $contain++;
        if (isset($_SESSION["shopping_cart"])) {
            $item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
            if (!in_array($_GET["id"], $item_array_id)) {
                $count = count($_SESSION["shopping_cart"]);
                $item_array = array(
                    'item_id' => $_GET["id"], 'item_name' => $_POST["hidden_name"], 'item_price' => $_POST["hidden_price"],
                    'item_quantity_person' => $_POST["quantity_person"], 'item_quantity_nights' => $_POST["quantity_nights"],
                    'item_date_arrive' => $_POST["date_arrive"]
                );
                $_SESSION["shopping_cart"][$count] = $item_array;
            } else {
                echo '<script> alert("Položka už bola pridaná!") </script>';
                echo '<script>window.location="rezervacie.php" </script>';
            }
        } else {
            $item_array = array(
                'item_id' => $_GET["id"], 'item_name' => $_POST["hidden_name"], 'item_price' => $_POST["hidden_price"],
                'item_quantity_person' => $_POST["quantity_person"], 'item_quantity_nights' => $_POST["quantity_nights"],
                'item_date_arrive' => $_POST["date_arrive"]
            );
            $_SESSION["shopping_cart"][0] = $item_array;
        }
    } else {
        echo '<script> alert("Už je pridaná jedna položka, nemôžete pridať ďalšiu.") </script>';
        echo '<script>window.location="rezervacie.php" </script>';
    }
}

/* Vymazanie apartmánov z nákupného košíka */
if (isset($_GET["action"])) {
    if ($_GET["action"] == "delete") {
        foreach ($_SESSION["shopping_cart"] as $keys => $values) {
            if ($values["item_id"] == $_GET["id"]) {
                unset($_SESSION["shopping_cart"][$keys]);
                echo '<script>alert("Položka bola odstránená")</script>';
                echo '<script>window.location="rezervacie.php"</script>';
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WebStranka</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600&
    subset=latin,latin-ext">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="jquery-3.5.1.min.js"></script>
    <script src="js/script1.js"></script>
    <script src="js/vlastny.js"> </script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<header>
    <div class="contact-bar">
        <div class="container">
            <ul class="menu personal">
                <?php
                if (!isLoggedIn()) {
                    echo "<li><a href=prihlasenie.php>Prihlásiť sa </a></li>";
                    echo "<li><a href=registracia.php>Vytvoriť účet</a></li>";
                }
                ?>
                <div class="content">
                    <?php if (isset($_SESSION['success'])) : ?>
                            <h3>
                                <?php
                                echo $_SESSION['success'];
                                unset($_SESSION['success']);
                                ?>
                            </h3>
                    <?php endif ?>
                    <div class="profile_info">
                        <div>
                            <?php if (isset($_SESSION['user'])) : ?>
                                <strong><?php echo $_SESSION['user']['username']; ?></strong>
                                <i style="color: #888;">(<?php echo ucfirst($_SESSION['user']['user_type']); ?>)</i>
                                <br>
                                <a href="index.php?logout='1'" style="color: blueviolet;">Odhlásiť sa</a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </ul>
            <ul class="menu date">
                <body onload="mojaFunkcia()">
                <div id="datum"></div>
                <div id="den_v_tyzdni"></div>
                </body>
            </ul>
        </div>
    </div>
    <div class="nav-bar">
        <div class="container">
            <h1 class="logo">
                <a href="#"></a>
            </h1>
            <nav class="group">
                <ul class="menu navigation">
                    <li><a href="index.php"> <i class="fa fa-home fa-2x"> </i> Ubytovanie </a></li>
                    <li class="selected"><a href="rezervacie.php"> <i class="fa fa-newspaper-o fa-2x"> </i> Rezervácia
                        </a></li>
                    <li><a href="recenzie.php"> <i class="fa fa-comment fa-2x"> </i> Recenzie </a></li>
                    <li><a href="konto.php"> <i class="fa fa-info-circle fa-2x"> </i> Moje konto </a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<main>
    <article>
        <header class="post-header">
            <div class="container">
                <h1 class="post-title">Rezervácia</h1>
            </div>
        </header>
        <div class="post-content">
            <div class="container">
                <?PHP
                $query = " SELECT `title`, `image`, `max_person_num`, `room_num`, `price_person_night` FROM `apartmany` order by id ASC ";
                $queryfire = mysqli_query($connect, $query);
                $num = mysqli_num_rows($queryfire);
                $datePom = date('Y-m-d');
                if ($num > 0) {
                    while ($hotel = mysqli_fetch_array($queryfire)) {
                        ?>
                        <div class="col-lg-3 col-md-5 col-sm-12">
                            <br>
                            <form method="post" action="rezervacie.php?action=add&id= <?php // echo $product['id']; ?>">
                                <div class="card">
                                    <h4 class="card-title bg-danger text-white p-2 text-uppercase"> <?php echo
                                        $hotel['title']; ?>   </h4>
                                    <div class="card-body">
                                        <a href="<?php echo $hotel['image']; ?>">
                                            <img src="<?php echo
                                            $hotel['image']; ?>" alt="apartman" class="img-fluid mb-2">
                                        </a>
                                        <h6> Max počet osôb: <?php echo $hotel['max_person_num'] ?></h6>
                                        <h6> Počet izieb: <?php echo $hotel['room_num'] ?></h6>
                                        <h6> Cena osoba/noc: <?php echo $hotel['price_person_night'] ?> €</h6>
                                        <h6> Počet osôb: </h6>
                                        <input type="number" name="quantity_person" class="form-control" value="1"
                                               min="1" max="<?php echo $hotel['max_person_num']; ?>"/>
                                        <h6> Počet nocí: </h6>
                                        <input type="number" name="quantity_nights" class="form-control" value="1"
                                               min="1" max="7"/>
                                        <h6> Dátum príchodu: </h6>
                                        <input type="date" name="date_arrive" class="form-control" min="<?php echo $datePom ?>"
                                               value="<?php echo $datePom ?>"/>
                                        <input type="hidden" name="hidden_name"
                                               value="<?php echo $hotel["title"]; ?>"/>
                                        <input type="hidden" name="hidden_price"
                                               value="<?php echo $hotel["price_person_night"]; ?>"/>
                                        <br>
                                        <div class="btn-group d-flex">
                                            <button class="btn btn-info flex-fill" type="submit" name="add_to_cart">
                                                Pridať do košíka
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <?php
                    }
                }
                ?>
                <div style="clear:both"></div>
                <br/>
                <h3 class="res">Detaily Vašej rezervácie</h3>
                <div class="row justify-content-center">
                    <table class="table table-bordered">
                        <tr>
                            <th width="25%">Názov apartmánu</th>
                            <th width="15%">Dátum príchodu</th>
                            <th width="10%">Počet osôb</th>
                            <th width="10%">Počet nocí</th>
                            <th width="20%">Cena</th>
                            <th width="5%"></th>
                            <th width="5%"></th>
                        </tr>
                        <?php
                        if (!empty($_SESSION["shopping_cart"])) {
                            foreach ($_SESSION["shopping_cart"] as $keys => $values) {
                                ?>
                                <tr>
                                    <?php $nameRez = $values["item_name"]; ?>
                                    <?php $number = number_format($values["item_quantity_person"] * $values["item_price"] * $values["item_quantity_nights"], 2); ?>
                                    <?php $date_arrive = $values["item_date_arrive"]; ?>
                                    <?php $num_person = $values["item_quantity_person"]; ?>
                                    <?php $num_nights = $values["item_quantity_nights"]; ?>
                                    <td><?php echo $nameRez; ?> </td>
                                    <td><?php echo $date_arrive; ?></td>
                                    <td><?php echo $num_person; ?></td>
                                    <td><?php echo $num_nights; ?></td>
                                    <td> <?php echo $number; ?> €</td>
                                    <td>
                                        <a href="rezervacie.php?action=delete&id=<?php echo $values["item_id"]; ?>"><span
                                                    class="text-danger">Vymazať</span></a></td>
                                    <td>
                                        <a href="rezervacie.php?action=send&id=<?php echo $values["item_id"]; ?>"><span
                                                    class="text-info">Odoslať</span></a></td>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </article>
</main>
<footer class="footer">
    <div class="container">
        <ul class="menu nav-footer">
            <li><a href="index.php"> Ubytovanie </a></li>
            <li><a href="rezervacie.php"> Rezervácia </a></li>
            <li><a href="recenzie.php"> Recenzie </a></li>
            <li><a href="konto.php"> Moje konto </a></li>
        </ul>
    </div>
</footer>
</body>
</html>
<?php
/* Akcia pre odoslanie rezervácie do tabuľky rezervacie v databáze*/
if (isset($_GET["action"])) {
    if ($_GET["action"] == "send") {
        $loginId = $_SESSION['user']['id'];
        $sql = "INSERT INTO rezervacie (title, date_arrive, num_person, num_nights, price, login_id)
                   VALUES ('$nameRez','$date_arrive','$num_person', '$num_nights', '$number' ,'$loginId')";
        mysqli_query($connect, $sql);
        foreach ($_SESSION["shopping_cart"] as $keys => $values) {
            if ($values["item_id"] == $_GET["id"]) {
                unset($_SESSION["shopping_cart"][$keys]);
                echo '<script>alert("Objednávka bola odoslaná.")</script>';
                echo '<script>window.location="rezervacie.php"</script>';
            }
        }
    }
}
?>