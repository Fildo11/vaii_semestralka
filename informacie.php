<?php include('sendRegistracia.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WebStranka</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600&
    subset=latin,latin-ext">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="jquery-3.5.1.min.js"></script>
    <script src="js/vlastny.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<header>
    <div class="contact-bar">
        <div class="container">
            <ul class="menu personal">
                <?php
                if (!isLoggedIn()) {
                    echo "<li><a href=prihlasenie.php>Prihlásiť sa </a></li>";
                    echo "<li><a href=registracia.php>Vytvoriť účet</a></li>";
                }
                ?>
                <div class="content">
                    <?php if (isset($_SESSION['success'])) : ?>
                        <h3>
                            <?php
                            echo $_SESSION['success'];
                            unset($_SESSION['success']);
                            ?>
                        </h3>
                    <?php endif ?>
                    <div class="profile_info">
                        <div>
                            <?php if (isset($_SESSION['user'])) : ?>
                                <strong><?php echo $_SESSION['user']['username']; ?></strong>
                                <i style="color: #888;">(<?php echo ucfirst($_SESSION['user']['user_type']); ?>)</i>
                                <br>
                                <a href="index.php?logout='1'" style="color: blueviolet;">Odhlásiť sa</a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </ul>
            <ul class="menu date">
                <body onload="mojaFunkcia()">
                <div id="datum"></div>
                <div id="den_v_tyzdni"></div>
                </body>
            </ul>
        </div>
    </div>
    <div class="nav-bar">
        <div class="container">
            <h1 class="logo">
                <a href="#"></a>
            </h1>
            <nav class="group">
                <ul class="menu navigation">
                    <li class="selected"><a href="index.php"> <i class="fa fa-home fa-2x"> </i> Ubytovanie </a></li>
                    <li><a href="rezervacie.php"> <i class="fa fa-newspaper-o fa-2x"> </i> Rezervácia </a></li>
                    <li><a href="recenzie.php"> <i class="fa fa-comment fa-2x"> </i> Recenzie </a></li>
                    <li><a href="konto.php"> <i class="fa fa-info-circle fa-2x"> </i> Moje konto </a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<main>
    <article>
        <header class="post-header">
            <div class="container">
                <h1 class="post-title">Ponuka ubytovania</h1>
            </div>
        </header>
        <div class="post-content">
            <div class="container">
                <h5 class="text-center"> Vitajte na webovej stránke nášho apartmánu. Dozviete sa tu základné informácie
                    o ponuke ubytovania, cene za ubytovanie a pod. Zároveň si tu môžete priamo zarezervovať ubytovanie
                    podľa vášho výberu, prípadne pridať recenzie ako ste boli s našími službami spokojný.</h5>
                <ul class="menu controls">
                    <li><a href="index.php">Galéria</a></li>
                    <li><a href="podujatia.php">Podujatia</a></li>
                    <li class="selected"><a href="informacie.php">Informácie</a></li>
                </ul>

                <div class="questions">
                    <h4> Kde sa nachádzame? </h4>
                </div>
                <div class="answer">
                    <p> Nachádzame sa v okrese Nové Mesto nad Váhom, na odľahlej ploche, ktorá ponuká príjemné pokojné
                        prostredie. Ak sa ku nám neviete dostať tak nás pokojne kontaktujte, prípadne využite
                        internetové mapy :) </p>
                </div>

                <div class="questions">
                    <h4> Ako sa ku nám dostanete? </h4>
                </div>
                <div class="answer">
                    <p> Najlepší spôsob príchodu je samozrejme autom, pred areálom sa nachádza veľké parkovisko pre cca
                        50 aút. Ďalší spôsob príchodu je vlakom ku železničnej stanici v Novom Meste nad Váhom a odtial
                        vás čaká asi 4 km túra pešo... Alebo si môžete zobrať taxík :)</p>
                </div>

                <div class="questions">
                    <h4> Aké ubytovania ponúkame? </h4>
                </div>
                <div class="answer">
                    <p> Celkovo ponúkame 8 ubytovacích zariadení (od chatiek, cez penzióny, apartmány až po vilu). Cena
                        za noc na osobu sa u každého ubytovania líši podľa jeho kvality. Presné ceny aj ďalší popis
                        ubytovaní (maximálny počet osôb, počet izieb, foto ubytovania,...) nájdete v ponuke Rezervácia.
                        Pre vstup do tejto menu ponuky však musíte byť zaregistrovaný a prihlásený v našom systéme.</p>
                </div>

                <div class="questions">
                    <h4> Aké služby ponúkame? </h4>
                </div>
                <div class="answer">
                    <p> Okrem ubytovania, ponúkame aj možnosť stravovania sa v našej reštaurácii formou raňajok, obedov
                        a večerí. Navyše každý mesiac máme u nás naplánovanú minimálne jednu akciu na ktorej sa môžete
                        zúčastniť a zabaviť sa. Zoznam akcií nájdete v podmenu Podujatia na tejto stránke. </p>
                </div>
            </div>
        </div>
    </article>
</main>
<footer class="footer">
    <div class="container">
        <ul class="menu nav-footer">
            <li><a href="index.php"> Ubytovanie </a></li>
            <li><a href="rezervacie.php"> Rezervácia </a></li>
            <li><a href="recenzie.php"> Recenzie </a></li>
            <li><a href="konto.php"> Moje konto </a></li>
        </ul>
    </div>
</footer>
</body>
</html>

<script>
    var pom = document.getElementsByClassName('questions');
    for (var i = 0; i < pom.length; i++) {
        pom[i].addEventListener('click', function () {
            this.classList.toggle('active');
            var udaje = this.nextElementSibling;
            if (udaje.style.maxHeight) {
                udaje.style.maxHeight = null;
            } else {
                udaje.style.maxHeight = udaje.scrollHeight + 'px';
            }
        })
    }
</script>