function mojaFunkcia() {
    var d = new Date();
    var den = d.getDate();
    var mesiac = d.getMonth() + 1;
    var rok = d.getFullYear();
    document.getElementById("datum").innerHTML = den + "." + mesiac + "." + rok;

    var denVTyzdni = d.getDay();
    var urciDen = "";
    if (denVTyzdni == 1) {
        urciDen = "Pondelok";
    } else if (denVTyzdni == 2) {
        urciDen = "Utorok";
    } else if (denVTyzdni == 3) {
        urciDen = "Streda";
    } else if (denVTyzdni == 4) {
        urciDen = "Štvrtok";
    } else if (denVTyzdni == 5) {
        urciDen = "Piatok";
    } else if (denVTyzdni == 6) {
        urciDen = "Sobota";
    } else {
        urciDen = "Nedela";
    }
    document.getElementById("den_v_tyzdni").innerText = "Dnes je: " + urciDen;
}