var ajax = new XMLHttpRequest();
var metoda = "GET";
var url = "info_podujatia.php";
var asynronne = true;
ajax.open(metoda, url, asynronne);
ajax.send();

ajax.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        var data = JSON.parse(this.responseText);
        var html = "";
        for (var i = 0; i < data.length; i++) {
            var name_event = data[i].name_event;
            var date_event = data[i].date_event;
            var popis = data[i].popis;
            html += "<td>" + name_event + "</td>";
            html += "<td>" + date_event + "</td>";
            html += "<td>" + popis + "</td>";
            html += "</tr>";
        }
        document.getElementById("data").innerHTML = html;
    }
}