<?php include "sendRegistracia.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WebStranka</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600&
    subset=latin,latin-ext">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="js/vlastny.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<header>
    <div class="contact-bar">
        <div class="container">
            <ul class="menu personal">
                <li><a href="prihlasenie.php">Prihlásiť sa </a></li>
                <li><a href="registracia.php">Vytvoriť účet</a></li>
            </ul>
            <ul class="menu date">
                <body onload="mojaFunkcia()">
                <div id="datum"></div>
                <div id="den_v_tyzdni"></div>
                </body>
            </ul>
        </div>
    </div>
    <div class="nav-bar">
        <div class="container">
            <h1 class="logo">
                <a href="#"></a>
            </h1>
            <nav class="group">
                <ul class="menu navigation">
                    <li><a href="index.php"> <i class="fa fa-home fa-2x"> </i> Ubytovanie </a></li>
                    <li><a href="rezervacie.php"> <i class="fa fa-newspaper-o fa-2x"> </i> Rezervácia </a></li>
                    <li><a href="recenzie.php"> <i class="fa fa-comment fa-2x"> </i> Recenzie </a></li>
                    <li><a href="konto.php"> <i class="fa fa-info-circle fa-2x"> </i> Moje konto </a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<main>
    <header class="post-header">
        <div class="container">
            <h1 class="post-title">Vytvorenie nového účtu</h1>
        </div>
    </header>
    <div class="post-content">
        <form method="post" action="registracia.php">
            <div class="group-in">
                <label>Nickname</label>
                <input type="text" name="username" minlength="5" value="" required>
            </div>
            <div class="group-in">
                <label>Email</label>
                <input type="email" name="email" value="" required>
            </div>
            <div class="group-in">
                <label>Telefónne číslo</label>
                <input type="tel" name="phone_number" value="" pattern="[0-9]{10}" maxlength="10"
                       title="Zadajte 10 miestne číslo" required>
            </div>
            <div class="group-in">
                <label>Dátum narodenia</label>
                <input type="date" name="date_birthday" value="" max="2009-01-01" required>
            </div>
            <div class="group-in">
                <label>Heslo</label>
                <input type="password" name="password_1" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                       maxlength="100"
                       title="Vaše heslo musí obsahovať aspoň 8 znakov, z toho aspoň jedno veľké písmeno, jedno malé
                           písmeno a jednu číslicu" required>
            </div>
            <div class="group-in">
                <label>Potvrdenie hesla</label>
                <input type="password" name="password_2" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                       maxlength="100"
                       title="Vaše heslo musí obsahovať aspoň 8 znakov, z toho aspoň jedno veľké písmeno, jedno malé
                           písmeno a jednu číslicu" required>
            </div>
            <div class="group-in">
                <button type="submit" class="btn" name="register_btn">Registrácia</button>
            </div>
        </form>
    </div>
</main>
<footer class="footer">
    <div class="container">
        <ul class="menu nav-footer">
            <li><a href="index.php"> Ubytovanie </a></li>
            <li><a href="rezervacie.php"> Rezervácia </a></li>
            <li><a href="recenzie.php"> Recenzie </a></li>
            <li><a href="konto.php"> Moje konto </a></li>
        </ul>
    </div>
</footer>
</body>
</html>