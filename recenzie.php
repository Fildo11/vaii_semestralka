<?php
include('sendRegistracia.php');
$update = false;
$id = 0;
$subjectRecenzia = '';
$messageRecenzia = '';
if (!isLoggedIn()) {
    // $_SESSION['msg'] = "Pre pridávanie recenzii musíš byť prihlásený";
    header('location: recenzieNeprihlasena.php');
}
?>

<?php
/* Výber recenzie podľa id, ktorú budeme chcieť editovať */
if (isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $update = true;
    $record = mysqli_query(mysqli_connect('localhost', 'root', 'dtb456', 'ubytovaci_web'),
        "SELECT * FROM recenzie WHERE id=$id");
    $pom = mysqli_fetch_array($record);
    $usernameRecenzia = $pom['username'];
    $subjectRecenzia = $pom['subject'];
    $messageRecenzia = $pom['mess'];
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WebStranka</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600&
    subset=latin,latin-ext">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/vlastny.js"></script>
    <script src="js/moj.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<header>
    <div class="contact-bar">
        <div class="container">
            <ul class="menu personal">
                <?php
                if (!isLoggedIn()) {
                    echo "<li><a href=prihlasenie.php>Prihlásiť sa </a></li>";
                    echo "<li><a href=registracia.php>Vytvoriť účet</a></li>";
                }
                ?>
                <div class="content">
                    <?php if (isset($_SESSION['success'])) : ?>
                        <h3>
                            <?php
                            echo $_SESSION['success'];
                            unset($_SESSION['success']);
                            ?>
                        </h3>
                    <?php endif ?>
                    <div class="profile_info">
                        <div>
                            <?php if (isset($_SESSION['user'])) : ?>
                                <strong><?php echo $_SESSION['user']['username']; ?></strong>
                                <i style="color: #888;">(<?php echo ucfirst($_SESSION['user']['user_type']); ?>)</i>
                                <br>
                                <a href="index.php?logout='1'" style="color: blueviolet;">Odhlásiť sa</a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </ul>
            <ul class="menu date">
                <body onload="mojaFunkcia()">
                <div id="datum"></div>
                <div id="den_v_tyzdni"></div>
                </body>
            </ul>
        </div>
    </div>
    <div class="nav-bar">
        <div class="container">
            <h1 class="logo">
                <a href="#"></a>
            </h1>
            <nav class="group">
                <ul class="menu navigation">
                    <li><a href="index.php"> <i class="fa fa-home fa-2x"> </i> Ubytovanie </a></li>
                    <li><a href="rezervacie.php"> <i class="fa fa-newspaper-o fa-2x"> </i> Rezervácia </a></li>
                    <li class="selected"><a href="recenzie.php"> <i class="fa fa-comment fa-2x"> </i> Recenzie </a></li>
                    <li><a href="konto.php"> <i class="fa fa-info-circle fa-2x"> </i> Moje konto </a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<main>
    <article>
        <header class="post-header">
            <div class="container">
                <h1 class="post-title">Recenzie</h1>
            </div>
        </header>
        <div class="post-content">
            <div class="container">
                <form method="post" action="sendRegistracia.php">
                    <input type="hidden" name="id" value="<?php echo $id ?>">
                    <div class="group-in">
                        <label>Nickname</label>
                        <input type="text" name="nameRecenzia" value="<?php echo $_SESSION['user']['username']; ?>"
                               readonly>
                    </div>
                    <div class="group-in">
                        <label>Predmet</label>
                        <input type="text" name="subjectRecenzia" value="<?php echo $subjectRecenzia ?>" required>
                    </div>
                    <div class="group-in">
                        <label>Správa</label>
                        <input type="text" name="messageRecenzia" value="<?php echo $messageRecenzia ?>" required>
                    </div>
                    <div class="group-in">
                        <?php
                        if ($update == true):
                            ?>
                            <button type="submit" class="btn btn-info" name="updateRecenzia">Upraviť</button>
                        <?php else: ?>
                            <button class="btn" type="submit" name="saveRecenzia"> Uložiť</button>
                        <?php endif; ?>
                    </div>
                </form>
                <div class="container">
                    <?php
                    $myDB = new mysqli('localhost', 'root', 'dtb456', 'ubytovaci_web') or die (mysqli_eror($myDB));
                    $result = $myDB->query("SELECT * FROM recenzie") or die($myDB->error);
                    ?>
                    <div class="row justify-content-center">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nickname</th>
                                <th>Predmet</th>
                                <th>Správa</th>
                            </tr>
                            </thead>
                            <?php
                            while ($row = $result->fetch_assoc()): ?>
                                <tr>
                                    <td><?php echo $row['username']; ?></td>
                                    <td><?php echo $row['subject']; ?></td>
                                    <td><?php echo $row['mess']; ?></td>
                                    <td>
                                        <?php
                                        if ($_SESSION['user']['id'] == $row['login_id']) {
                                            ?>
                                            <a href="sendRegistracia.php?delete=<?php echo $row['id']; ?>"
                                               class="btn btn-danger" title="Vymazať"> <i class="fa fa-remove"> </i>
                                            </a>
                                            <a href="recenzie.php?edit=<?php echo $row['id']; ?>"
                                               class="btn btn-info" title="Upraviť"> <i class="fa fa-edit"> </i> </a>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php endwhile; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </article>
</main>
<footer class="footer">
    <div class="container">
        <ul class="menu nav-footer">
            <li><a href="index.php"> Ubytovanie </a></li>
            <li><a href="rezervacie.php"> Rezervácia </a></li>
            <li><a href="recenzie.php"> Recenzie </a></li>
            <li><a href="konto.php"> Moje konto </a></li>
        </ul>
    </div>
</footer>
</body>
</html>